import { Component, OnInit } from '@angular/core';
import { CartapiService } from '../services/cartapi.service';
import { DataService } from '../services/data.service'; // Import the CartapiService (assuming it's correct)
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  totalItemNumber: number = 0;
  products: any = [];
  accountUserName: any = '';
  accountUserRole: any = "";

  businessSettings: boolean = false;
  logout: boolean = false;
  account: boolean = false;
  signin: boolean = false;



  constructor(private cartApi: CartapiService, private dataService: DataService, private router: Router, private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.cartApi.getProductData().subscribe(res => {
      this.products = res;
      this.totalItemNumber = this.cartApi.getTotalqnt();
      //this.totalItemNumber=this.cartApi.ttqn;
      //console.log("ttqn header is: "+this.cartApi.ttqn);

      //this.totalItemNumber=this.dataService.getSharedVariable3()
      //console.log("ggtotalitemnumber is: "+this.totalItemNumber);
      //console.log("gg is: "+this.cartApi.getTotalqnt());
      //console.log("ggshared is: "+this.dataService.getSharedVariable3());


      //this.totalItemNumber=this.cartApi.getTotalQuantity();
      //this.totalItemNumber=this.dataService.getSharedVariable3();
      //console.log("this total item number"+this.totalItemNumber);

      // Get JWT token from local storage
  

      // Get user details from local storage and parse it as an object
      const userDetails = localStorage.getItem('userDetails');
      const username = localStorage.getItem('username');
      const userrole = localStorage.getItem('userrole');
      const useremail = localStorage.getItem('useremail');

      // Get userType from local storage
      const userType = localStorage.getItem('userType');


      console.log("from header"+userDetails);
      console.log("from header"+username);
      console.log("from header"+userrole);
      console.log("from header"+useremail);

      this.accountUserName=username;
      this.accountUserRole=userrole;
    

      // Now you can use jwtToken, userDetails, and userType in your application

      if (this.accountUserRole === "ADMIN" || this.accountUserRole === "AGENT") {
        this.businessSettings = true;
        this.logout = true;
        this.account = true;
        this.signin = false;
      }else{
        if (this.accountUserRole === "CUSTOMER") {
          this.businessSettings = false;
          this.logout = true;
          this.account = true;
          this.signin = false;
        } else {
          this.businessSettings = false;
          this.logout = false;
          this.account = false;
          this.signin = true;
        }
        

      }

    })

    console.log(this.accountUserName);
    
  }

  logOut(){
    this.auth.logout();
    this.router.navigate(['/login']);
  }

  signIn(){
    this.auth.logout();
    this.router.navigate(['/login']);
  }

}
