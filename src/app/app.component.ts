import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NgxPaginationModule } from 'ngx-pagination';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoginRoute: boolean = false;
  isSignupRoute: boolean = false;
  isResetPasswordRoute: boolean = false;
  isSendEmail: boolean = false;


  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        const routeData = this.route.firstChild?.snapshot.data;

        // Check if the login or signup data property is true
        this.isLoginRoute = routeData?.['login'] || false;
        this.isSignupRoute = routeData?.['signup'] || false;
        this.isResetPasswordRoute = routeData?.['resetpassword'] || false;
        this.isSendEmail = routeData?.['sendemail'] || false;

      });
  }
}


