import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import jwt_decode from 'jwt-decode'; // Import the jwt-decode library
import { JwtPayload } from '../models/jwt-payload.interface';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  data: any[] = [];
  userRole: any;

  type: string = "password";
  isText: boolean = false;
  eyeIcon: string = "bi bi-eye-slash-fill";
  loginForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private api: ApiService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required ]
    });

    // ... (other initialization code)
  }

  hideShowPassword() {
    this.isText = !this.isText;
    this.isText ? this.eyeIcon = "bi bi-eye-fill" : this.eyeIcon = "bi bi-eye-slash-fill";
    this.isText ? this.type = "text" : this.type = "password";
  }



  getUserFormData(data: any) {
  

    this.authService.clientLogin(data).subscribe((result) => {

      console.log("User logged in successfully");
      //console.log(data);
      //console.log(result);
      //console.log('userDetails', JSON.stringify(result.user));
      console.log('userDetails', result.user);
      console.log('username', result.user.username);
      
      this.userRole = result.user.role;

      console.log('user role', this.userRole);
      console.log('jwt', result.jwt);



      console.log('user email', JSON.stringify(result.user.email));

      // Save JWT token and user details in local storage
      //localStorage.setItem('jwtToken', result.jwt);
      localStorage.setItem('userDetails', JSON.stringify(result.user));
      localStorage.setItem('username', result.user.username);
      localStorage.setItem('userrole', result.user.role);
      localStorage.setItem('useremail', result.user.email);
      //localStorage.setItem('jwt', result.jwt);


      localStorage.setItem('userType', result.userType); // If you want to store userType as well

      // Store the JWT token in an HttpOnly cookie
      this.authService.setSessionToken(result.jwt);
      //this.authService.setSessionToken(result.user.role);


    

      //this.router.navigate(['/']);

      this.checkSessionExpiry();

      if (this.userRole=="CUSTOMER") {
        this.router.navigate(['/products']);
      }
      if (this.userRole=="ADMIN") {
        this.router.navigate(['/item-management']);
      }

     

      

    },
      (error) => {

        console.error("Login failed:", error);
      }

      //NB//
      //Remember to handle cases where the local storage data might be missing or invalid, and to clear the local storage when the user logs out or when the token expires for security reasons.
    );
  }

  // In your auth service
  




  checkSessionExpiry() {
    // Get the token from local storage
    const token = this.api.getSessionToken();

    // Check if a token is present
    if (token) {
      // Decode the token using the JwtPayload interface for type assertion
      const tokenPayload = jwt_decode(token) as JwtPayload; // Use type assertion


      // Calculate expiration time in milliseconds
      const expirationTimeInSeconds = tokenPayload.exp; // Expiration time from backend in seconds
      
      const expirationTimeInMilliseconds = expirationTimeInSeconds * 1000; // Convert to milliseconds

    
      // Get the current time in milliseconds
      const currentTime = Date.now();

      // Compare the current time with the expiration time
      if (currentTime > expirationTimeInMilliseconds) {
        // If the token is expired, log the user out
        this.authService.logout();
        this.router.navigate(['/login']);
      }
    }
  }

  noLogin(){
    localStorage.setItem('username', "Account");
    localStorage.setItem('userrole', "undefined");
    this.router.navigate(['/products']);

  }

















}
