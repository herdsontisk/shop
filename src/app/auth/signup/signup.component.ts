import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  data: any[] = [];


  type: string = "password";
  isText: boolean = false;
  eyeIcon: string = "bi bi-eye-slash-fill";

  typeRetype: string = "password";
  isTextRetype: boolean = false;
  eyeIconRetype: string = "bi bi-eye-slash-fill";


  signUpForm!: FormGroup;

  constructor(private fb: FormBuilder,private api: ApiService, private authService: AuthenticationService, private router: Router){}

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      username: ['',Validators.required],
      password:['', Validators.required]
    })

  }

  hideShowPassword(){
    this.isText = !this.isText;
    this.isText ? this.eyeIcon = "bi bi-eye-fill" : this.eyeIcon = "bi bi-eye-slash-fill";
    this.isText ? this.type = "text" : this.type = "password";
  }

  retypeHideShowPassword(){
    this.isTextRetype = !this.isTextRetype;
    this.isTextRetype ? this.eyeIconRetype = "bi bi-eye-fill" : this.eyeIconRetype = "bi bi-eye-slash-fill";
    this.isTextRetype ? this.typeRetype = "text" : this.typeRetype = "password";
  }

  onSignUp(){
    console.log(this.signUpForm.value);
    
    this.authService.signUp(this.signUpForm.value)
    .subscribe({
      next:(res=>{
        alert("signup success")

      }),
      error:(err=>{
        alert(err?.error.message)
      })
    })
  }



  getUserFormDataSignUp(data: any) {
    
    this.authService.signUp(data).subscribe((result) => {
      console.log(data);
      console.log(result);
      console.log("mesage " + result.message)
      console.log("mesage " + result.timestamp)
      console.log("mesage " + result.data)
      console.log("mesage " + result.data.role)

      


        // Save JWT token and user details in local storage
        //localStorage.setItem('jwtToken', result.jwt);
        //localStorage.setItem('userDetails', JSON.stringify(result.user));
        //localStorage.setItem('userType', result.userType); // If you want to store userType as well
  
        console.log("account created successfully");

        this.router.navigate(['/login']);
      },
      (error) => {
        console.error("signup failed:", error);
      }

      //NB//
      //Remember to handle cases where the local storage data might be missing or invalid, and to clear the local storage when the user logs out or when the token expires for security reasons.
    );
  }







}
