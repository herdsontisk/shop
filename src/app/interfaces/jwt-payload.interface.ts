export interface JwtPayload {
    exp: number;
     // Expiration time as a UNIX timestamp
    // Include other properties from your JWT payload if needed
  }