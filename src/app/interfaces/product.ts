// product.interface.ts (create a new file for the interface)
export interface Product {
    title: string;
    description: string;
    price: number;
  
  }
  
