import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.css']
})
export class AccountProfileComponent implements OnInit {

  details: any;
  name: any;
  role: any;
  email: any;



  constructor(private router: Router){}

  ngOnInit(): void {



    const userDetails = localStorage.getItem('userDetails');
    const username = localStorage.getItem('username');
    const userrole = localStorage.getItem('userrole');
    const useremail = localStorage.getItem('useremail');

    if (username===null) {
      this.router.navigate(['/login']);
    }

    this.details = userDetails;
    this.name = username;
    this.role = userrole;
    this.email = useremail;

  
    
  }

}
