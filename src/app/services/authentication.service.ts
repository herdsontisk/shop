import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  constructor(private http: HttpClient) { }

  setSessionToken(token: string) {
    // Set the session token in an HttpOnly cookie
    document.cookie = `sessionToken=${token}; path=/`;
  }

  

    // In your auth service
    logout() {
      // Clear the session token (remove the cookie)
      document.cookie = 'sessionToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
  
      // Clear other cached data if needed
      localStorage.removeItem('userDetails');
      localStorage.removeItem('userType');
      localStorage.removeItem('jwt');
      localStorage.removeItem('username');
      localStorage.removeItem('userrole');
      localStorage.removeItem('useremail');
  
      // ... Perform any other necessary cleanup or logout actions
    }





  signUp(data: any): Observable<any>{
    return this.http.post<any>(`http://192.168.100.37:8080/shop/user-management/signup`, data);    

  }

  clientLogin(data: any): Observable<any>{
    return this.http.post<any>(`http://192.168.100.37:8080/shop/auth/user-login`, data);

  }

  sendEmail(data: any): Observable<any>{
    return this.http.post<any>(`http://192.168.100.37:8080/shop/auth/reset-link`, data);

  }

  resetPassword(data: any): Observable<any>{
    return this.http.post<any>(`http://192.168.100.37:8080/shop/auth/reset-link`, data);

  }


}
