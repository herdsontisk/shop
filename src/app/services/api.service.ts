import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseUrl = 'http://192.168.100.37:8080/shop/shop-item';


  constructor(private http:HttpClient) { }



  addItem(data: any){

   const jwt = this.getSessionToken()

   //const jwt = localStorage.getItem('jwt');
   console.log("this my jwt in header"+ jwt);

   const headers = new HttpHeaders({
    'Authorization': `Bearer ${jwt}`,
  });
   
    //return this.http.post(this.baseUrl,data);
    return this.http.post(`${this.baseUrl}/add-new-Items`, data, { headers });    

  }




  getSessionToken(): string | null {
    const cookies = document.cookie.split(';');
    for (const cookie of cookies) {
      const parts = cookie.trim().split('=');
      if (parts[0] === 'sessionToken') {
        return parts[1];
      }
    }
    return null;
  }

  getItems(){

    return this.http.get(`${this.baseUrl}/get-all-Items`).pipe(map((res:any)=>{
      return res.data
     
    }))
  }


  getItemsTest(){
    return this.http.get<any>(`http://192.168.100.37:8080/shop/shop-item/get-all-Items`)
     
  }


  signUp(data: any): Observable<any>{
    return this.http.post<any>(`http://192.168.100.37:8080/shop/user-management/signup`, data);    

  }


  getItemById(){

  }

  updateItem(id: number,data: any){

    const jwt = this.getSessionToken()

    //const jwt = localStorage.getItem('jwt');
    console.log("this my jwt in header"+ jwt);
 
    const headers = new HttpHeaders({
     'Authorization': `Bearer ${jwt}`,
   });
   

    this.http.put('http://192.168.100.37:8080/shop/shop-item/'+id,data,{ headers })
    .subscribe();
  }

  deleteAllItems(){

    const jwt = this.getSessionToken()

    //const jwt = localStorage.getItem('jwt');
    console.log("this my jwt in header"+ jwt);
 
    const headers = new HttpHeaders({
     'Authorization': `Bearer ${jwt}`,
   });

    this.http.delete('http://192.168.100.37:8080/shop/shop-item', { headers })
    .subscribe();

  }

  deleteItemById(id: number){

    const jwt = this.getSessionToken()

    //const jwt = localStorage.getItem('jwt');
    console.log("this my jwt in header"+ jwt);
 
    const headers = new HttpHeaders({
     'Authorization': `Bearer ${jwt}`,
   });

    this.http.delete('http://192.168.100.37:8080/shop/shop-item/'+id, { headers })
    .subscribe();

  }

  //auth//



}
