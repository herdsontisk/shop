import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../services/data.service'; // Import HttpClient

@Injectable({
  providedIn: 'root'
})
export class CartapiService {
  cartDataList: any[] = [];
  productList = new BehaviorSubject<any[]>([]);
  ttqn: number = 0;
  totalSingle: number = 0;
  grandTotalSingle: number = 0;

  constructor(private httpClient: HttpClient, private dataService: DataService) { }

  getProductData() {
    return this.productList.asObservable();
  }

  setProduct(product: any) {
    this.cartDataList.push(product);
    this.productList.next(this.cartDataList);
  }

  addToCart(product: any) {

    this.cartDataList.push(product);
    this.productList.next(this.cartDataList);
    this.getTotalAmount();
    console.log(this.cartDataList);
  }

  addToCartExist(product: any) {
    // Check if the product already exists in the cart
    const existingProduct = this.cartDataList.find((item: any) => item.id === product.id);

    if (existingProduct) {
      // If the product exists, increase the quantity

      existingProduct.quantity += 1;
      this.productList.next(this.cartDataList);
      this.getTotalAmount();
      this.getTotalqnt();
      existingProduct.total += product.price
      //this.grandTotalAmountSingle();
      //this.getTotalSingleAlternative();


      //this.getTotalqnt();
      //console.log("return on"+this.getTotalqnt());

      //this.newgtotal = this.getTotalqnt();

    } else {
      // If the product is not in the cart, add it with quantity 1

      this.cartDataList.push(product);
      this.productList.next(this.cartDataList);
      this.getTotalAmount();
      this.getTotalqnt();
      //existingProduct.total += product.price

      //this.getTotalSingleAlternative();
      //this.grandTotalAmountSingle();

      console.log(this.cartDataList);
      console.log("new " + this.getTotalqnt());
      console.log("amount" + this.getTotalAmount());
      
    }
  }

  quantityIncrementCart(product: any) {
    //const existingProduct = this.cartDataList.find((item: any) => item.id === product.id);

    product.quantity += 1;
    this.productList.next(this.cartDataList);
    this.getTotalqnt();
    this.getTotalAmount();
    product.total += product.price

  }

  quantitydecrementCart(product: any) {

    if(product.quantity == 1){


    }else{
      product.quantity -= 1;
      this.productList.next(this.cartDataList);
      this.getTotalqnt();
      this.getTotalAmount();
      product.total -= product.price

    }

  }

  getTotalqnt(): number {
    let grandTotalqn = 0;
    this.cartDataList.map((a: any) => {
      grandTotalqn += a.quantity;
    });
    //console.log("tt is: " + grandTotalqn);
    this.ttqn = grandTotalqn;
    //console.log("ttqn is: " + this.ttqn);
    this.dataService.setSharedVariable3(this.ttqn);

    return grandTotalqn
  }

  // Assuming cartDataList contains products with a 'quantity' property representing the quantity of each product in the cart.
  // Assuming cartDataList contains products with a 'quantity' property representing the quantity of each product in the cart.
  getTotalQuantity(): number {
    let totalQuantity = 0;

    // Iterate through the cartDataList and add up the quantity of each product.
    for (const product of this.cartDataList) {
      if (product.quantity && !isNaN(product.quantity)) {
        totalQuantity += product.quantity;
      }
    }

    return totalQuantity;
  }

  getTotalAmount(): number {
    let grandTotal = 0;
    this.cartDataList.map((a: any) => {
      grandTotal += a.price * a.quantity;
    });
    console.log("log for grandtotal" + grandTotal);

    return grandTotal
  }

  grandTotalAmountSingle(): number {

    for (const product of this.cartDataList) {
      this.totalSingle = product.quantity * product.price;
    }

    return this.totalSingle
  }

  getTotalSingleAlternative(): number {
    this.cartDataList.map((a: any) => {
      this.grandTotalSingle = a.price * a.quantity;
    });

    return this.grandTotalSingle
  }







  removeCartData(product: any) {
    this.cartDataList.map((a: any, index: number) => {
      if (product.id === a.id) {
        this.cartDataList.splice(index, 1);
      }
    });
    this.productList.next(this.cartDataList)
  }

  removeAllCart() {
    this.cartDataList = [];
    this.productList.next(this.cartDataList);
  }
}
