import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { FooterComponent } from './footer/footer.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatBadgeModule } from '@angular/material/badge';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { MensFashionComponent } from './mens-fashion/mens-fashion.component';
import { SummaryPipe } from './pipes/summary.pipe';
import { CheckoutComponent } from './checkout/checkout.component';
import { ItemManagementComponent } from './item-management/item-management.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PopupComponent } from './popup/popup.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AccountProfileComponent } from './account-profile/account-profile.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SendemailComponent } from './sendemail/sendemail.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbCollapseModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

 // Import the MatDialogModule

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsComponent,
    CartComponent,
    FooterComponent,
    ProductDetailsComponent,
    MensFashionComponent,
    SummaryPipe,
    CheckoutComponent,
    ItemManagementComponent,
    PopupComponent,
    Test2Component,
    LoginComponent,
    SignupComponent,
    AccountProfileComponent,
    ResetpasswordComponent,
    SendemailComponent,
    
    
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatBadgeModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    NgxPaginationModule,
    NgbModule,
    NgbCollapseModule,
    
  ],

 
  bootstrap: [AppComponent]
})
export class AppModule { }
