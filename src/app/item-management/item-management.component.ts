import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { CartapiService } from '../services/cartapi.service';
import { DataService } from '../services/data.service';
import { MatDialog, MatDialogContent } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { PopupComponent } from '../popup/popup.component';
import { Router } from '@angular/router';
import { Product } from '../interfaces/product';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxPaginationModule } from 'ngx-pagination'; 
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from '../services/authentication.service';



declare var $: any;

@Component({
  selector: 'app-item-management',
  templateUrl: './item-management.component.html',
  styleUrls: ['./item-management.component.css']
})
export class ItemManagementComponent {

  title = "Angular Server Post Data Test"

  page: number = 1;

  productList: any[] = [];
  itemman: any;
  data: any[] = [];
  editModal: boolean = false;
  currentProductId: any;
  idSingleItem: any;
  titleSingleItem: any;

  tableUpdater: boolean = false;

  showDiv: boolean = false;
  showDivAll: boolean = false;

  showDivAddToDataBase: boolean = false;
  showDivUpdated: boolean = false;

  closeModalAdd: boolean = false;
  closeModalUpdate: boolean = false;

  testmethod: boolean = false;

  isButtonDisabled: boolean = false;

  searchTerm: string = '';

  jwt: any;


  currentPage: number = 1;
  itemsPerPage: number = 10; // Adjust as needed 

  itemsPerPageNav: number = 1; // Adjust as needed 


  isCollapsed = true;


  isText: boolean = true;
  eyeIcon: string = "bi bi-funnel";

  eyeIconDrop:string = "bi bi-chevron-down"

  iconChange: string = "bi bi-chevron-down"

  
  


  dataSource: any;
  displayedColumns:string[]=["id","number","title","category","image","description","price","action"]


  @ViewChild('usersForm') form!: NgForm;

  constructor(private router: Router,private api: ApiService, private cartApi: CartapiService, private dataService: DataService, private dialog: MatDialog, private http: HttpClient, private authService: AuthenticationService,
    ) { 

  }
  


  ngOnInit(): void {

    const username = localStorage.getItem('username');

    if (username===null) {
      this.router.navigate(['/login']);
    }


      this.api.getItems().subscribe((res) => {
        this.productList = res;
        this.data = res;
        console.log("this this the response "+ res);
        //console.log("this this the response "+ this.productList);

 

        console.log(this.editModal);
      });
    


  };

  isCollapsedMethod(){
    this.isCollapsed=false;
    this.isText = !this.isText;
    this.isText ? this.eyeIcon = "bi bi-funnel" : this.eyeIcon = "bi bi-x-square";
    this.isText ? this.eyeIconDrop = "bi bi-chevron-down" : this.eyeIconDrop = "bi bi-chevron-up";

    }



  hideShowPassword() {
    this.isText = !this.isText;
    this.isText ? this.eyeIcon = "bi bi-funnel" : this.eyeIcon = "bi bi-x-square";
    this.isText ? this.eyeIconDrop = "bi bi-chevron-down" : this.eyeIconDrop = "bi bi-chevron-up";

  }

  buttonClickIconChange(){
    this.isText = !this.isText;
    this.isText ? this.iconChange = "bi bi-chevron-down" : this.iconChange = "bi bi-chevron-up";
  }



  get filteredData() {
    return this.data.filter(item =>
      item.category.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      item.title.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      //item.price.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      item.description.toLowerCase().includes(this.searchTerm.toLowerCase())

    )
    .slice((this.currentPage - 1) * this.itemsPerPage, this.currentPage * this.itemsPerPage);

  }

  onPageChange(pageNumber: number) {
    this.currentPage = pageNumber;
    return this.data.filter(item =>
      item.category.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      item.title.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      //item.price.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      item.description.toLowerCase().includes(this.searchTerm.toLowerCase())

    )
    .slice((this.currentPage - 1) * this.itemsPerPage, this.currentPage * this.itemsPerPage);

    //this.filteredData();
  }

  get totalPageArray(): number[] {
    return Array.from({ length: Math.ceil(this.filteredData.length / this.itemsPerPage) }, (_, index) => index + 1);
  }


  handleClick() {
    if (!this.isButtonDisabled) {
      this.isButtonDisabled = true;
      const customDelayInMilliseconds = 300000;
      setTimeout(() => {
        this.isButtonDisabled = false;
      }, customDelayInMilliseconds);
    }
  }

  opennew() {
    this.testmethod = true;
    console.log(this.testmethod);
    //$('#myModalx').modal('show');
  }

  reopen() {
    //this.isButtonDisabled = false;
    this.api.getItems().subscribe((res) => {
      this.productList = res;
      this.data = res;
    });
  }

  reopenClose() {
    this.isButtonDisabled = false;
    this.api.getItems().subscribe((res) => {
      this.productList = res;
      this.data = res;
    });
  }

  openModalDeleteWarning() {
    this.showDivAll = false;
    //$('#myModalDeleteWarning').modal('show');
  }
  closeModalDeleteWarning() {
    $('#myModalDeleteWarning').modal('hide');
    this.reopen();
    this.isButtonDisabled = false;
    // Use Angular method to hide the modal
  }

  openMyModalDeleteWarningSingleItem(id: string, title: string) {
    this.showDiv = false;
    console.log(id + title);
    this.idSingleItem = id;
    this.titleSingleItem = title;
    //$('#myModalDeleteWarningSingleItem').modal('show');

  }
  closeMyModalDeleteWarningSingleItem() {
    this.showDiv = false;
    $('#myModalDeleteWarningSingleItem').modal('hide');
    this.reopen();
    this.isButtonDisabled = false;

  }

  closeModalForUpdate() {
    this.showDivUpdated = false;
    $('#myModal').modal('hide');
    this.reopen();
    this.isButtonDisabled = false;
    // Use Angular method to hide the modal
  }

  openModalEdit() {
    $('#myModal').modal('show');
    this.closeModalAdd = false;
  }

  openModal() {
    this.clearForm();
    //$('#myModal').modal('show');
  }
  clearForm() {
    this.form.setValue({
      number: "",
      title: "",
      category: "",
      description: "",
      price: "",
      image: "",
    })
    this.editModal = false;
    this.showDivUpdated = false;
    this.showDivAddToDataBase = false;
    this.closeModalUpdate = false,
      this.closeModalAdd = true;
    console.log(this.editModal);
    //this.number = ''; // Clear the form field values
    // You can clear other form fields in a similar way
    this.form.resetForm(); // Reset the entire form
  }

  closeModal() {
    $('#myModal').modal('hide');
    this.reopen();
    this.isButtonDisabled = false;
    // Use Angular method to hide the modal
  }

  closeModalForAdd() {
    this.showDivAddToDataBase = false;
    $('#myModal').modal('hide');
    this.reopen();
    this.isButtonDisabled = false;
  }

  getUserFormData(data: any) {
    if (!this.editModal) {
      this.api.addItem(data).subscribe((result) => {
        console.log(data);
        console.log(result);
        this.showDivAddToDataBase = true;
        this.showDivUpdated = false;
        this.reopen();
        this.handleClick();
        console.log("item added successfully");
      });

    } else {
      this.api.updateItem(this.currentProductId, data)
      this.showDivUpdated = true;
      this.reopen();
      this.handleClick();
      console.log("item updated successfully");
    }
  }

  addtoDetails(item: any) {
    this.dataService.setSharedVariable1(item);
    this.dataService.setSharedVariable2(this.productList);

  }
  deleteProduct() {
    this.showDiv = true;
    this.api.deleteItemById(this.idSingleItem);
    this.reopen();
    console.log("item deleted successfully");
    this.reopen();
    this.handleClick();

  }

  deleteAllProduct() {
    this.showDivAll = true;
    this.api.deleteAllItems();

    this.handleClick();
    this.reopen();
    console.log("all items deleted, therefore your shop is empty");
  }

  editProduct(id: number) {
    this.showDivUpdated = false;
    this.closeModalAdd = false;
    this.closeModalUpdate = true;
    this.editModal = true;
    console.log(this.editModal);
    this.currentProductId = id;
    let currentProduct = this.productList.find((p) => { return p.id === id });
    //this.openModalEdit();
    console.log(currentProduct);

    this.form.setValue({
      number: currentProduct.number,
      title: currentProduct.title,
      category: currentProduct.category,
      description: currentProduct.description,
      price: currentProduct.price,
      image: currentProduct.image
    });
  }

  openPopup() {
    this.dialog.open(PopupComponent)

  }

}
