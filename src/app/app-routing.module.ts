import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { MensFashionComponent } from './mens-fashion/mens-fashion.component';
import { ItemManagementComponent } from './item-management/item-management.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AccountProfileComponent } from './account-profile/account-profile.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SendemailComponent } from './sendemail/sendemail.component';




const routes: Routes = [
  {path:'', redirectTo:'products', pathMatch:'full'},
  {path:'products', component:ProductsComponent},
  {path:'cart', component:CartComponent},
  {path:'product-details', component:ProductDetailsComponent},
  {path:'mens-fashion', component:MensFashionComponent},
  {path:'item-management', component:ItemManagementComponent},
  {path:'test2', component:Test2Component},
  {path:'account-profile', component:AccountProfileComponent},
  


  {
    path: 'sendemail',
    component: SendemailComponent,
    data: { sendemail: true } // Indicate that this route is related to login
  },
  
  {
    path: 'resetpassword',
    component: ResetpasswordComponent,
    data: { resetpassword: true } // Indicate that this route is related to login
  },

  {
    path: 'login',
    component: LoginComponent,
    data: { login: true } // Indicate that this route is related to login
  },
  
  {
    path: 'signup',
    component: SignupComponent,
    data: { signup: true } // Indicate that this route is related to signup
  }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
